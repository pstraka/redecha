<?php
if (!defined('_PS_VERSION_'))
  exit;
 
class BlockDoprava extends Module
{
  public function __construct()
  {
    $this->name = 'blockdoprava';
    $this->tab = 'front_office_features';
    $this->version = '1.0.0';
    $this->author = 'Ing. Pavel Straka';
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.4', 'max' => _PS_VERSION_); 
    $this->bootstrap = true;
 
    parent::__construct();
 
    $this->displayName = $this->l('Doprava');
    $this->description = $this->l('Modul pro výpočet dopravy');
 
    $this->confirmUninstall = $this->l('Opravdu chcete modul smazat?');
 
    if (!Configuration::get('blockdoprava'))      
      $this->warning = $this->l('No name provided');
  }



public function install()
{
  if (Shop::isFeatureActive())
    Shop::setContext(Shop::CONTEXT_ALL);
 
  if ( parent::install() && $this->registerHook('leftColumn')  && $this->registerHook('displayProductSecondaryColumn'))

    return true;
 
  return false;
}

public function uninstall()
{
  if (!parent::uninstall() ||
    !Configuration::deleteByName('blockdoprava')
  )
    return false;
 
  return true;
}

public function hookDisplayProductSecondaryColumn() {

            $cena_kosiku = Context::getContext()->cart->getOrderTotal(false,Cart::BOTH_WITHOUT_SHIPPING);
           
            $id_shop = (int)Context::getContext()->shop->id;    // id shopu
            $id_product = $_GET['id_product'];
            $product = new Product($id_product,false,2);
            
            $cena_produktu = Product::getPriceStatic($id_product);
            
            foreach (Product::getProductAttributesIds($id_product) as $id_product_attribute){
			       $ids_product_attribute[] = $id_product_attribute['id_product_attribute'];
             }
            
            /*
            if ( $product->price > 30 or $cena_produktu_sleva  > 30){
            echo 'pod 30';
            }else{
            echo 'nad 30';
            }
            */
             
            //echo $product->quantity_discount;
            //$dostupnost = $product->checkQty();
            //Product::getRealQuantity($id_product, $id_product_attribute = 0, $id_warehouse = 0, $id_shop = null)            
            $dostupny = $product->available_now;

      
            $warehouses_product =  Warehouse::getProductWarehouseList($_GET['id_product']);
            $vyrazy = ['Skladom ','Skladem'];
      

            $warehouses_stock = array();
            
            if (!empty($warehouses_product))
            {
                foreach ($warehouses_product  as $wp)
                {
                   $warehouse_data = array();
                   $warehouse_data['name'] = $wp['name'];
                   $warehouse_data['stock'] = Product::getRealQuantity((int)$_GET['id_product'], 0, $wp['id_warehouse']);
                   $warehouses_stock[] = $warehouse_data;
                }
               if (!empty($stock_warehouses)){}
                  $this->context->smarty->assign('dostupnost',$warehouses_stock[0]['stock']);
            }
          

			if (empty($ids_product_attribute)){			 
            if((empty($stock_warehouses) && $warehouses_stock[0]['stock'] <= 0)){
             $skladem_od = $product->available_later;

             preg_match('/(\d{2})\.(\d{2})\.(\d{4})/', $skladem_od, $matches);

              if (count($matches) == 4){
                  $datum =  $matches[3].'-'.$matches[2].'-'.$matches[1];            
                }							           
                else{
                 preg_match('/(\d)/', $skladem_od, $matches);
                 $datum = date('Y-m-d',strtotime('+'.$matches[0].' days'));                
                } 
            }
						 else{
              $datum = FALSE; 
            }
          }
          else{
          	$allowed_warehouse_for_combination = WareHouse::getProductWarehouseList((int)$id_product, (int)$id_product_attribute, (int)$id_shop);
          }
						
    
            if($id_shop == 2){   // česká verze
            $doprava_nadpis = ['CENY A TERMÍNY PŘEPRAVY','Dodání do','Dodání dnes do'];
            $doruceni =  doruceni($cena_produktu,$cena_kosiku) ;
            
                        
            }
            else if($id_shop == 1){
            $doprava_nadpis = ['CENY A TERMÍNY PREPRAVY','Dodanie do','Dodanie dnes do'];
         
            $doruceni =  doruceni_sk($cena_produktu,$cena_kosiku) ;
            }
            //echo '<pre>';print_r($product);'</pre>';   //out_of_stock
             /*
            $customer = $this->context->cookie->id_customer;
            $shoppingListObj = new ShoppingListObject();
            $shoppingList = $shoppingListObj->getByIdCustomer($customer);
            
            $product = new Product(Tools::getValue('id_product'));
          
            $this->context->smarty->assign('title', $product->name[1]);*/
            $this->context->smarty->assign('doprava_nadpis', $doprava_nadpis); 
            $this->context->smarty->assign('sluzby', $doruceni);
            $this->context->smarty->assign('datum_doruceni', vypocet_casu($id_shop,$datum,$warehouses_stock[0]['stock']));    
            return $this->display(__FILE__, 'views/templates/doprava_cz.tpl');
        
    }
}
function vypocet_casu($id_shop,$datum,$skladem){

$velikonoce = date('m-d', easter_date(date('Y'))+(60*60*24)); 
$velky_patek = date('m-d', easter_date(date('Y'))-(60*60*48));


if($id_shop == 2) {
$nonWorkingDays = array('01-01', '05-01','05-08','07-05','07-06','09-28','10-28','11-17','12-24','12-25','12-26',$velikonoce,$velky_patek);
$doruceni =  doruceni() ;
}
else if($id_shop == 1){
$nonWorkingDays = array('01-01', '01-06','05-01','07-05','08-29','09-01','11-01','11-17','12-24','12-25','12-26',$velikonoce,$velky_patek);
$doruceni = doruceni_sk();
}

$nextDates = []; // Empty array to hold the next 3 dates
$aktualni = date('Y-m-d');
          
    foreach($doruceni as $index => $sluzby){
    $run=0; 
    $date = new DateTime($datum);
    $addingInterval = new DateInterval('P1D'); 

    //$addingInterval = new DateInterval('P1D');
      if(time() <  mktime($sluzby[3],0,0,date('m'),date('d'),date('Y')) && !in_array(date ('w'),[0,6])){
      $daysToAdd = $sluzby[1];
      }
      else{
      $daysToAdd = $sluzby[2];
      }

    $cyklo = FALSE;
    do{ 
    if($index == 'Cyklokuriér BA' && ($skladem < 0 or $skladem == 0) && !in_array(date ('w'),[0,6])){
    //$addingInterval = new DateInterval('P1D');
    $hlaska = ($skladem == 0) ? 'doobeda' : FALSE;
    $cyklo = $datum;
    }
    else{
    if($index == 'Cyklokuriér BA' && time() <  mktime(16,0,0,date('m'),date('d'),date('Y')) && !in_array(date ('w'),[0,6])){
    $addingInterval = new DateInterval("PT3H"); 
    $hour = date('H',time() + 3*60*60);
    $minute = (date('i')>30)?'30':'00';
    $cyklo = $hour.':'.$minute;
    $hlaska = 'dnes';    
    }
    
    elseif($index == 'Cyklokuriér BA' && time() > mktime(16,0,0,date('m'),date('d'),date('Y')) && !in_array(date ('w'),[0,6])){
    //$addingInterval = new DateInterval("PT3H");
    $hlaska = 'doobeda';     
    //$addingInterval = new DateInterval('P1D');
    }
    
    else{
    //$addingInterval = new DateInterval('P1D');
    $hlaska = FALSE;
    }
    }
    $date->add($addingInterval); // add that one day
    if(in_array($date->format('w'),[0,6]) || in_array($date->format('m-d'),$nonWorkingDays)){
    
      continue; // day is weekend or non working day, skip it

    }
 
    $run++; // day is working day, add it

} while($run < $daysToAdd); 
    

    $nextDates[$index] = [$date->format('d.m.Y '),$hlaska,$cyklo]; 
  
    }  

return $nextDates;
}

function doruceni($cena = FALSE,$kosik = FALSE){

$zadarmo = '0 Kč';
  if($cena > 1000 or $kosik > 1000){
    $cc = $u =  $zadarmo;
  }
  else{
  $cc = '59 Kč';
  $u = '39 Kč';
  }

$sluzby = [
'Česká pošta' => [$cc,3,4,15],
'Uloženka' => [$u,3,4,15],
'Kurýr DPD' => ['79 Kč',2,3,15],
];

return $sluzby;
}

function doruceni_sk($cena = FALSE,$kosik = FALSE){

$zadarmo = '0 €';
  if($cena > 30 or $kosik> 30){
    $cc = $zadarmo;
  }
  else{
  $cc = '1,5 €';

  }

$sluzby = [
'Osobný odber BA' => ['0 €',1,2,21],
'Slovenská pošta' => [$cc,2,3,14],
'Kuriér SPS' => ['2,9 €',1,2,14],
'Cyklokuriér BA' => ['6 €',0,1,15]
];

return $sluzby;
}


            

